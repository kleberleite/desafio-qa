package br.com.concreteSolutionTesteQA;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SolutionTesteQATeste {
	
	@Test
	public void testeDeCalculo(){

		assertEquals(0,ConcreteSolutionTesteQA.calDePreco(""));
		assertEquals(50,ConcreteSolutionTesteQA.calDePreco("A"));
		assertEquals(80,ConcreteSolutionTesteQA.calDePreco("A,B"));
		assertEquals(115,ConcreteSolutionTesteQA.calDePreco("C,D,B,A"));
		assertEquals(100,ConcreteSolutionTesteQA.calDePreco("A,A"));
		assertEquals(130,ConcreteSolutionTesteQA.calDePreco("A,A,A"));
		assertEquals(180,ConcreteSolutionTesteQA.calDePreco("A,A,A,A"));
		assertEquals(230,ConcreteSolutionTesteQA.calDePreco("A,A,A,A,A"));
		assertEquals(260,ConcreteSolutionTesteQA.calDePreco("A,A,A,A,A,A"));
		assertEquals(160,ConcreteSolutionTesteQA.calDePreco("A,A,A,B"));
		assertEquals(175,ConcreteSolutionTesteQA.calDePreco("A,A,A,B,B"));
		assertEquals(190,ConcreteSolutionTesteQA.calDePreco("A,A,A,B,B,D"));
		assertEquals(190,ConcreteSolutionTesteQA.calDePreco("D,A,B,A,B,A"));
		

	}

}